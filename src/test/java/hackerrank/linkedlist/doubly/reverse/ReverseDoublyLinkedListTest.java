package hackerrank.linkedlist.doubly.reverse;

import hackerrank.linkedlist.LinkedListAssert;
import hackerrank.linkedlist.doubly.DoublyLinkedList;
import hackerrank.linkedlist.doubly.DoublyLinkedListNode;
import org.junit.jupiter.api.Test;

import static hackerrank.linkedlist.doubly.reverse.ReverseDoublyLinkedList.reverse;

class ReverseDoublyLinkedListTest {

    @Test
    void exampleTestCase() {
        DoublyLinkedList linkedList = new DoublyLinkedList();
        linkedList.insertNode(1);
        linkedList.insertNode(2);
        linkedList.insertNode(3);
        linkedList.insertNode(4);

        DoublyLinkedListNode reversedHead = reverse(linkedList.head);

        LinkedListAssert.assertThat(reversedHead).hasNodesExactly(4, 3, 2, 1);
    }

}