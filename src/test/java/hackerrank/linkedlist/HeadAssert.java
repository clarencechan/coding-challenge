package hackerrank.linkedlist;

import hackerrank.linkedlist.doubly.DoublyLinkedListNode;
import org.assertj.core.api.Assertions;

public class HeadAssert {

    private DoublyLinkedListNode head;

    public HeadAssert(final DoublyLinkedListNode head) {
        this.head = head;
    }

    public void hasNodesExactly(int... values) {
        for (int value : values) {
            Assertions.assertThat(head.data).isEqualTo(value);
            head = head.next;
        }
    }
}
