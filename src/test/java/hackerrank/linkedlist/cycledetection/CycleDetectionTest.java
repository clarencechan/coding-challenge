package hackerrank.linkedlist.cycledetection;

import hackerrank.linkedlist.singly.SinglyLinkedList;
import hackerrank.linkedlist.singly.SinglyLinkedListNode;
import org.junit.jupiter.api.Test;

import static hackerrank.linkedlist.singly.cycledetection.CycleDetection.hasCycle;
import static org.assertj.core.api.Assertions.assertThat;

class CycleDetectionTest {

    @Test
    public void testGivenExampleNoCycle() {
        SinglyLinkedList linkedList = new SinglyLinkedList();
        linkedList.insertNode(1);

        addTailAndCycle(linkedList, 1, 3);
        assertThat(hasCycle(linkedList.head)).isFalse();
    }

    @Test
    public void testGivenExampleHaveCycle() {
        SinglyLinkedList linkedList = new SinglyLinkedList();
        linkedList.insertNode(1);
        linkedList.insertNode(2);
        linkedList.insertNode(3);
        linkedList.insertNode(2);

        addTailAndCycle(linkedList, 4, 2);
        assertThat(hasCycle(linkedList.head)).isTrue();
    }

    // This is to append a tail at the end of the linked list
    // cycleIndex refers to the position for creating a cycle
    // If cycleIndex and linkedListCount have never met, there is no cycle
    private void addTailAndCycle(SinglyLinkedList linkedList, int linkedListCount, int cycleIndex) {
        SinglyLinkedListNode extra = new SinglyLinkedListNode(-1);
        SinglyLinkedListNode temp = linkedList.head;

        for (int i = 0; i < linkedListCount; i++) {
            if (i == cycleIndex) {
                extra = temp;
            }

            if (i != linkedListCount - 1) {
                temp = temp.next;
            }
        }
        temp.next = extra;
    }
}