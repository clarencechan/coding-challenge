package hackerrank.linkedlist.doubly.reverse;

import hackerrank.linkedlist.doubly.DoublyLinkedListNode;

public class ReverseDoublyLinkedList {

    public static DoublyLinkedListNode reverse(DoublyLinkedListNode llist) {
        if (llist == null || llist.next == null) {
            return llist;
        }

        DoublyLinkedListNode head = reverse(llist.next);
        llist.next.next = llist;
        llist.prev = llist.next;
        llist.next = null;
        return head;
    }

}
