package hackerrank.linkedlist.singly.cycledetection;

import hackerrank.linkedlist.singly.SinglyLinkedListNode;

import java.util.HashSet;
import java.util.Set;

public class CycleDetection {

    public static boolean hasCycle(SinglyLinkedListNode head) {
        Set<SinglyLinkedListNode> nodeSet = new HashSet<>();
        SinglyLinkedListNode pointer = head;
        while (pointer.next != null) {
            boolean isAdded = nodeSet.add(pointer);
            if (!isAdded) {
                return true;
            }
            pointer = pointer.next;
        }
        return false;
    }
}
